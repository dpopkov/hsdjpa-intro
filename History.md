## History

* JPA Entities
    * learn.hsdjpa.hsdjpaintro.domain.Book
    * Method equals() that compares just the id - It is discouraged solution (JPWH,10.3.2)
* Spring Data Repositories ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#4387ca37da0f492497e71cb5c50cb410))
    * Extend `org.springframework.data.jpa.repository.JpaRepository`
* Initialize data ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#5974ebe7e6bf4cc48c537382b2923dd5))
    * Extend `org.springframework.boot.CommandLineRunner`
* SQL logging ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#5226350bb32e47c3a3855556b9adf7a0))
    * to show with bind parameters as question marks, add to application.properties `spring.jpa.show-sql=true`
    * or to make output more user-friendly:
        * Show SQL: `spring.jpa.properties.hibernate.show_sql=true`
        * Format SQL: `spring.jpa.properties.hibernate.format_sql=true`
        * Show bind parameters: `logging.level.org.hibernate.type.descriptor.sql=trace`
* H2 Database console ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#a53241d65027457c9ec686e41bafc0ca))
    * Configured automatically if using Developer Tools
    * Or can be enabled in application.properties: `spring.h2.console.enabled=true`
* MySQL script to create database and users
* Spring Boot Test
* Spring Boot JPA Test Slice
* Using Test Transactions
    * Ordered tests
        * @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
        * @Order(1) @Test
    * To use result of test in the next test you can apply annotations
        * `@Rollback(value = false)` or
        * `@Commit`
* Bootstrapping data in tests
* MySQL Spring Boot configuration ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#ba8c865c0948476099daf3b22973db36))
    * add `mysql-connector-java` to pom.xml
    * create application-local.properties
        * spring.datasource.username=${BOOKDB_USER}
        * spring.datasource.password=${BOOKDB_PASSWORD}
        * spring.datasource.url=jdbc:mysql://127.0.0.1:3306/bookdb?userUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
        * spring.jpa.database=mysql
        * spring.jpa.hibernate.ddl-auto=update
* Integration Test for MySQL ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#3416a9a51b514fcaa9dd1af53b62b5a7))
    * use `@Profile({"local", "default"})` for DataInitializer
    * add MySqlIntegrationTest
        * use `@ActiveProfiles({"local"})`
        * override using default embedded H2 by Spring Boot: 
            * use `@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)`
* H2 MySQL Compatibility Mode ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#8f2ebabda8b4455886c0915c1c850fa6))
    * move `spring.jpa.database=mysql` from application-local to application properties file
    * by default Spring Boot is auto-configuring in-memory database for DataJpaTest 
        * so apply `@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)` to override it
    * use `MODE=MYSQL` in h2 data source url for MySQL compatibility mode
* Schema initialization with Hibernate ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#6c2f6a4eea6e4bf2b9c829e58fc54b83))
    * use sql creation statements from running SpringBootJpaSliceTest for creating `schema.sql`
    * change `ddl-auto` to `validate` 
* Schema initialization with MySQL ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#4ed5b72ec6bb4782acd2d9a983cab3d5))
    * edit `application-local.properties`:
        * change ddl-auto from update to __validate__
        * add `spring.sql.init.mode=always` so this is going to always run the schema.sql initialization script
        * run MySqlIntegrationTest to be sure - the IDs should be incremented from the beginning because of table recreation
* Use H2 for Spring Boot Application ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#c4c38105d22f4f9eb2840f8a37ee8c9a))
    * add _Maven Profile_ for H2 database using `test` scope
    * enable that Profile in IDE
    * run the application without "local" Spring Boot _Active Profile_ and enjoy H2 database instead of MySQL
* Liquibase Maven Plugin ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#59c3062363fc40d78aee8ca0a4f78d38))
    * add `liquibase-maven-plugin` to pom.xml
    * configure url, username and password
    * add mysql-connector dependency to plugin
* Generate changeSets from Database ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#ba672c701fe84404886c0c39a15c90bf))
    * add `outputChangeLogFile` to liquibase-maven-plugin configuration
    * add `changeSetAuthor` and `changelogSchemaName`
    * run `mvn liquibase:generateChangeLog`
* Organizing Change Logs in Liquibase project ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#c1e39dd5ee034516905bb9f38fa41c7c))
    * create directory `resources/db/changelog`
    * add master file `db.changelog-master.xml`
    * use the previously generated changelog as `db.baseline-changelog.xml`
* Spring Boot Configuration for Liquibase ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#0d13ffd4684048c2bc47cc4f8c258047))
    * add `liquibase-core` dependency to pom xml
    * add `spring.liquibase.change-log` property to application.properties
    * add Liquibase config to application-local.properties
        * `spring.liquibase.user=${BOOKDB_ADMIN}`
        * `spring.liquibase.password=${BOOKDB_ADMIN_PASSWORD}`
    * move `schema.sql` from resources to scripts folder
    * if run application now then tables will be created but hibernate_sequence is empty yet (it needs next value)
        * will throw `org.hibernate.id.IdentifierGenerationException: could not read a hi value - you need to populate the table: hibernate_sequence`
* Initializing data with Spring and Liquibase ([note](https://www.notion.so/Hibernate-and-Spring-Data-JPA-c6e94f30f1c444748e55f9ee662e9955#8103b4eb067643c18a97c63d64a67453))
    * add `db.init-hibernate-changelog.xml`
        * containing `<sql>insert into hibernate_sequence values (0)</sql>` SQL statement
    * edit changelog master:
        * add `<include  file="db/changelog/db.init-hibernate-changelog.xml"/>`
