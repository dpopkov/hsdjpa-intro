package learn.hsdjpa.hsdjpaintro.bootstrap;

import learn.hsdjpa.hsdjpaintro.domain.Book;
import learn.hsdjpa.hsdjpaintro.repositories.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({"local", "default"})
@Component
public class DataInitializer implements CommandLineRunner {
    private final BookRepository bookRepository;

    public DataInitializer(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void run(String... args) {
        bookRepository.deleteAll();

        Book bookDdd = new Book("Domain Driven Design", "123", "RandomHouse");
        Book savedDdd = bookRepository.save(bookDdd);

        Book bookSia = new Book("Spring in Action", "234", "Manning");
        Book savedSia = bookRepository.save(bookSia);

        bookRepository.findAll().forEach(book -> {
            System.out.print("Book Id: " + book.getId());
            System.out.print(", Book Title: " + book.getTitle());
            System.out.println(", Book ISBN: " + book.getIsbn());
        });
    }
}
